package de.embots.touchflow.module.implementation.modify;

import org.jdom.Element;

import de.embots.touchflow.exceptions.ModulException;
import de.embots.touchflow.exceptions.ModulFactoryException;
import de.embots.touchflow.gui.components.optionpane.Attribute;
import de.embots.touchflow.gui.components.optionpane.NumberAttribute;
import de.embots.touchflow.gui.components.optionpane.OptionPane;
import de.embots.touchflow.module.core.ModifyModule;
import de.embots.touchflow.module.core.PinName;
import de.embots.touchflow.module.pin.InputPin;
import de.embots.touchflow.module.pin.InputPin3D;
import de.embots.touchflow.module.pin.OutputPin;
import de.embots.touchflow.module.pin.OutputPin3D;
import de.embots.touchflow.util.KinectPoint;

public class Relativator3DTime extends ModifyModule {
	
	KinectPoint referencePoint=new KinectPoint();
	double abtastrate=300;
	long lastupdate=0;
	
	@Override
	public String getDescription() {
		return "<html>Outputs a difference vector to the last state a timespan before. The timespan can be adjusted in the settings.</html>";
	}
	
	@Override
	public void reinit(Attribute[] args) {
		abtastrate=(Double) args[0].getContent();
	}
	
	@Override
	public String getModuleName() {
		// TODO Auto-generated method stub
		return "Relativator3DTime";
	}

	@Override
	protected void processData() throws ModulException {
		

		double x=getInputPin(PinName.IN).getData();
		double y=getInputPin2D(PinName.IN).getData2();
		double z=getInputPin3D(PinName.IN).getData3();
		
		if (System.currentTimeMillis()-lastupdate> abtastrate){

			getOutputPin3D(PinName.OUT).writeData(x-referencePoint.x);
			getOutputPin3D(PinName.OUT).writeData2(y-referencePoint.y);
			getOutputPin3D(PinName.OUT).writeData3(z-referencePoint.z);
			referencePoint.x=x;
			referencePoint.y=y;
			referencePoint.z=z;
			lastupdate=System.currentTimeMillis();
		}
		
	}

	@Override
	public void openOptions() {
		NumberAttribute karg=new NumberAttribute("Abtastrate (ms)");
		karg.setContent(abtastrate);
		
		OptionPane.showOptionPane(new Attribute[]{karg},this);
	}
	
	@Override
	protected void additionalSaveAttribute(Element e) {
		e.setAttribute("Constructor",abtastrate+"");

	}

	@Override
	public void init(String params) throws ModulException {
		try{
			abtastrate=Double.parseDouble(params);
		}
		catch(Exception nf){
			throw new ModulFactoryException("Relativator3DTime:Konstruktorparam fehlt oder ist kein int");
		}
	}
	
	public Relativator3DTime() {
		super();
		inputPins=new InputPin[1];
		
		inputPins[0]=new InputPin3D(PinName.IN, this);

		
		outputPins=new OutputPin[1];
		
		outputPins[0]=new OutputPin3D(PinName.OUT, this);
	}



	
}
