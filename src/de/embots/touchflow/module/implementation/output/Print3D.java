package de.embots.touchflow.module.implementation.output;

import org.jdom.Element;

import de.embots.touchflow.exceptions.ModulException;
import de.embots.touchflow.module.core.OutputModule;
import de.embots.touchflow.module.core.PinName;
import de.embots.touchflow.module.pin.InputPin;
import de.embots.touchflow.module.pin.InputPin3D;

public class Print3D extends OutputModule
{

    public boolean printOnlyDifferent = true;
    private double oldVal;

    @Override
    protected void processData() throws ModulException
    {
        double newVal = getInputPin3D(PinName.IN).getData();
        double newVal2 = getInputPin3D(PinName.IN).getData2();
        double newVal3 = getInputPin3D(PinName.IN).getData3();



        if (!printOnlyDifferent || oldVal != newVal) {
            System.out.println(newVal + ";" + newVal2 + ";" + newVal3);
        }

        oldVal = newVal;

    }

    public Print3D()
    {
        inputPins = new InputPin[1];
        inputPins[0] = new InputPin3D(PinName.IN, this);

    }

    @Override
    public String getDescription()
    {
        return "<html>Outputs the values of <b>IN</b> on stdout, using ';' as a seperator</html>";
    }

    @Override
    protected void additionalSaveAttribute(Element e)
    {
        // TODO Auto-generated method stub
    }

    @Override
    public String getModuleName()
    {

        return "Print 3D";
    }
}
