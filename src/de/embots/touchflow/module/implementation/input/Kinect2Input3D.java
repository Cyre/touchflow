package de.embots.touchflow.module.implementation.input;

import java.io.InputStream;
import java.net.Socket;

import org.jdom.Element;

import de.embots.touchflow.exceptions.ModulException;
import de.embots.touchflow.module.core.InputModule;
import de.embots.touchflow.module.core.PinName;
import de.embots.touchflow.module.pin.OutputPin;
import de.embots.touchflow.module.pin.OutputPin3D;
import de.embots.touchflow.util.KinectPoint;

public class Kinect2Input3D extends InputModule {

	
	public Kinect2Input3D(){
		outputPins=new OutputPin3D[25];
		
		for (int i=0;i<outputPins.length;i++){
			outputPins[i]=new OutputPin3D(PinName.values()[PinName.SpineBase.ordinal()+i],this);
		}
				
		
	}
	
	@Override
	public String getDescription() {
		return "<html>Input from a Kinect 2 device.<br><br>Needs <i>Kinect2Sender</i> to be started!</html>";
	}
	
	@Override
	protected void processData() throws ModulException {
		for (int i=0; i<outputPins.length;i++){
			((OutputPin3D)outputPins[i]).writeDataVector(Kinect2Server.getPosition(i));
		}
	}

	@Override
	public String getModuleName() {
		return "Kinect2Input3D";
	}

	@Override
	protected void additionalSaveAttribute(Element e) {
		// TODO Auto-generated method stub

	}

}
