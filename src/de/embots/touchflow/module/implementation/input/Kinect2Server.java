package de.embots.touchflow.module.implementation.input;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.vecmath.Vector3d;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import de.embots.touchflow.exceptions.ModulException;
import de.embots.touchflow.module.Globals;
import de.embots.touchflow.module.core.Module;

public class Kinect2Server implements Runnable{
	
	/**
	 * Lauscht auf einen bestimmten Port umKinect-Bewegungen abzufangen
	 */
	
	private DatagramSocket server;

	
	static Vector3d[] positions=new Vector3d[25];
	
	private static Kinect2Server thisServer=new Kinect2Server();
	static{
		thisServer.init(2030);
	}
	
	private boolean running=true;
	private Thread thisThread;
	private ArrayList<Module> listeners=new ArrayList<Module>();


	private ServerSocket serverSocket;
		
	public void stopListening(Module listener){
		listeners.remove(listener);
		
		//no more listeners-> stop running & reset server
		if (listeners.size()==0) {
			running=false;
			server=null;
		}
	}
	
	/*
	 * default port: 2030
	 */
	public void init(int port){
		 try {
			serverSocket = new ServerSocket(2030,10);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Kinect 2 Server online, port:" + port);
		thisThread=new Thread(this);
		thisThread.start();
	}
	
	
	
	public static Vector3d getLeftHandPos() {
		return positions[7];
	}

	public static  Vector3d getRightHandPos() {
		return positions[11];
	}

	public void addListener(Module listener){
		listeners.add(listener);
	}
	@Override
	public void run() {
		try{
			while(running){
	            Socket connectionSocket = serverSocket.accept();
	            System.out.println("kinect2: accepted connection");
	            BufferedReader inFromClient =new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
	        
	            String clientSentence = inFromClient.readLine();
	            
	            clientSentence=clientSentence.replaceAll(",", ".");
	            
	            System.out.println("Received: " + clientSentence);
	
			
	            parseLine(clientSentence);
				
			}
			System.out.println("Kinect 2 server exited main loop");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * parse UDP message from MS client
	 * MSG Format: <JOINT_NAME:String>;<player num:int>;<xpos:float>;<ypos:float>;<zpos:float>
	 */
	public void parseLine(String msg){
		if (msg==null){
			System.err.println("Received null msg to parse from Kinect 2. Ignoring.");
			return;
		}
		String[] parts=msg.split(";");
		
	
		if (parts.length!=5){
			System.err.println("KinectServer-parse:invalid number of tokens:" + parts.length + " - msg: " + msg);
			return;
		}
		double x = 0,y = 0,z = 0;
		int player = 0;
		int jointNum=0;
		
		try{
			x=Double.parseDouble(parts[2]);
			y=Double.parseDouble(parts[3]);
			z=Double.parseDouble(parts[4]);
			player=Integer.parseInt(parts[1]);
			jointNum=Integer.parseInt(parts[0]);
		}
		catch(NumberFormatException nf){
			System.err.println("KinectServer-parse: number format error:" + msg);
			return;
		}
		
		/*if (player!=1){
			System.out.println("Currently, no second player is implemented.");
			return;
		}*/
		
		//ignore last joint, which is skeleton count
		if (jointNum <positions.length) positions[jointNum]=new Vector3d(x,y,z);
		
	}
	

	public static Vector3d getPosition(int i){
		Vector3d nullv=new Vector3d();
		if (i<0||i>=positions.length) return nullv;
		if(positions[i]!=null) return positions[i];
		return nullv;
	}
	

}
