﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Samples.Kinect.BodyBasics
{
    class MsgClient
    {

        public void send (int jointType, int player, double x, double y , double z)
        {
            String smsg = jointType + ";" + player + ";" + x + ";" + y + ";" + z + "\r\n";
            Console.Write("Sending msg: " + smsg);
            byte[] msg = Encoding.UTF8.GetBytes(smsg);
            int bytesSend = sock.Send(msg);
        }

        Socket sock = null;

        public void connect()
        {
            try
            {

                string host = "localhost";  // Uri
                int port = 2030; //standard port:2030

                IPHostEntry hostEntry = Dns.GetHostEntry(host);
                IPAddress[] ipAddresses = hostEntry.AddressList;

                Console.WriteLine(host + " is mapped to the IP-Address(es): ");

                // Ausgabe der zugeordneten IP-Adressen
                foreach (IPAddress ipAddress in ipAddresses)
                {
                    Console.Write(ipAddress.ToString());
                }

                // Instanziere einen Endpunkt mit der ersten IP-Adresse
                IPEndPoint ipEo = new IPEndPoint(ipAddresses[0], port);

                // IPv4 oder IPv6, Stream Socket, TCP
                sock = new Socket(ipEo.AddressFamily,
                                  SocketType.Stream,
                                  ProtocolType.Tcp);

                // Öffne eine Socket Verbindung
                sock.Connect(ipEo);

                // Prüfe ob eine Verbindung besteht?
                if (sock.Connected)
                {
                    Console.WriteLine(" - Connection established!\n");
                    // Socket schließen und Ressourcen freigeben
                    //sock.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void close()
        {
            if (sock!=null) sock.Close();
        }


    }
}
